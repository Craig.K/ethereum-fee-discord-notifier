## Ethereum Fee Discord Notifier

![](example.PNG)

This program will check Ethereum transaction fees and notify a Discord user
when they drop below a certain threshold. The Discord user is mentioned
in a server channel.

This program takes a numeric Discord user ID, a channel's webhook link 
(must be generated from within Discord), and an optional webhook link
for logs.

---

### Requirements:

   - Windows 10
   - Python 3.6+


### Setup:

1. Open the "env" file with a text or code editor and follow the instructions using channel-specific generated webhook links and your numeric Discord user ID
1. Install the following packages using pip3:

   - discord.py
   - requests
   - python-dotenv


### Start the program:

   1. Navigate to the program directory
   1. Double-click the file RUN.bat

----

### Ethereum Logo Attribution and License:

   https://ethereum.org/

   https://creativecommons.org/licenses/by-sa/4.0/

   I made changes to the Ethereum logo
