import os, discord, requests, json
from datetime import datetime, timedelta
# Imports environment variables from .env files
from dotenv import load_dotenv
from random import choice
from time import sleep


class DiscordNotifier():
    def __init__(self):
        os.system('cls')
        self.run_id = choice(range(1,9999))
        self.link = None
        self.max_fee = 20
        self.sleep_interval = 1800 # 30 mins
        self.today = None
        self.yesterday = None
        self._setupDiscord()
        self._setSleepInterval()
        self._setMaxFee()
        self.notify(f'You chose a **{int(self.sleep_interval/60)} minute** check interval and a **${self.max_fee} maximum fee**')
        self.mainloop()

    def mainloop(self):
        '''Run the application in a loop until the user ends the application'''

        active = True
        while active:
            average_fee = self._getAverageTransactionFee()
            if average_fee < self.max_fee:
                self.log(f'User notified of avg fee: {average_fee}')
                self.notify(f'''<@{os.getenv('DISCORD_USER_ID')}> **The average Ethereum transaction fee is ${int(average_fee)}!**''')
            else:
                self.log(f'The average Ethereum fee is {average_fee}')
            sleep(self.sleep_interval)

        self._fetchData()

    def notify(self, message):
        self.webhook.send(f'[{self.run_id}] {message}')

    def log(self, message):
        if self.webhook_log != '':
            self.webhook_log.send(f'[{self.run_id}] {message}')

    def _getAverageTransactionFee(self):
        '''Get the average transaction cost for the past day'''
        # Set the date so we can get the right information using the API
        self._setDate()
        # Update the API link with the current date
        self._setAPILink()

        # Request data using the website's API
        data = self._sendAPIRequest()

        if '<Response [5' in str(data):
            self.log(f'**Error!** {str(data)}: Internal server error (API error? Check link).  Lookup for more info')
        elif '<Response [429]' in str(data):
            self.log('**Error!** <Response [429]>: Too many requests')
        elif '<Response [403]' in str(data):
            self.log('**Error!** <Response [403]>: Forbidden')
        elif '<Response [401]' in str(data):
            self.log('**Error!** <Response [401]>: Unauthorized')
        elif '<Response [4' in str(data):
            self.log(f'**Error!** {str(data)}: 4XX (user error).  Lookup for more info.')
        else:
            # Store the average fee
            average_fee = data.json()['data']['values'][0][1]
            self.log(f'''fee: {average_fee}''')
            return average_fee

    def _sendAPIRequest(self):
        self.log(f'Fetching Ethereum data from {self.link}')
        return requests.get(self.link)

    def _setAPILink(self):
        self.link =  'https://data.messari.io/api/v1/assets/ethereum/metrics/txn.fee.avg/time-series?'
        self.link += f'start={self.yesterday.year}-{self.yesterday.month}-{self.yesterday.day}&'
        self.link += f'end={self.today.year}-{self.today.month}-{self.today.day}&interval=1d'


    def _setDate(self):
        self.today = datetime.now()
        self.yesterday = self.today - timedelta(days=1)

    def _setMaxFee(self):
        '''Ask the user for the maximum fee for notifications'''
        max_fee = 0

        while max_fee == 0:
            fee = input(f'Max fee [${self.max_fee}] >>> ')
            try:
                fee = int(fee)
                self.max_fee = fee
                self.log(f'self.max_fee set to {self.max_fee}')
                return
            except ValueError:
                confirm = input(f'Press "n" to cancel setting a ${self.max_fee} max fee >>> ')
                if confirm != "n" and confirm != "N":
                    self.log('Set default max fee')
                    max_fee = self.max_fee

    def _setSleepInterval(self):
        '''Ask the user for data fetch frequency'''
        sleep_interval = 0

        while sleep_interval == 0:
            interval = input(f'Minutes between API requests (2 min minimum) [{self.sleep_interval/60} mins] >>> ')
            try:
                interval = int(interval)
                self.sleep_interval = interval * 60
                self.log(f'self.sleep_interval set to {self.sleep_interval/60} minute(s)')
                return
            except ValueError:
                confirm = input(f'Press "n" to cancel setting a {self.sleep_interval/60} minute interval >>> ')
                if confirm != "n" and confirm != "N":
                    self.log('Set default interval')
                    sleep_interval = self.sleep_interval

    def _setupDiscord(self):
        '''Create webhooks using saved environment variables so we can send messages to the
           webhook and logs channel'''
        load_dotenv()

        # Setup notification webhook
        self.webhook = discord.Webhook.from_url(
            os.getenv('DISCORD_NOTIFIER_URL'), adapter=discord.RequestsWebhookAdapter()
                )
        self.notify('Discord notifier activated!')

        # Setup logging webhook
        log_url = os.getenv('DISCORD_NOTIFIER_LOGURL')
        if log_url != '':
            self.webhook_log = discord.Webhook.from_url(
                log_url, adapter=discord.RequestsWebhookAdapter()
                )
            self.log('Discord notifier logging activated')
        else:
            self.webhook_log = ''


if __name__ == '__main__':
    app = DiscordNotifier()